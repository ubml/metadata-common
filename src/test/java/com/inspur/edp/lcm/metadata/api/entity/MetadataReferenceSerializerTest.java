/*
 * Copyright (c) 2020 - present, Inspur Genersoft Co., Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */

package com.inspur.edp.lcm.metadata.api.entity;

import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.MapperFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.PropertyNamingStrategies;
import com.fasterxml.jackson.databind.SerializationFeature;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import java.util.ArrayList;
import java.util.UUID;

/**
 * 元数据的依赖关系{@link MetadataReference} 序列化方法{@link MetadataReferenceSerializer}单元测试
 *
 * @author sunhongfei
 */
class MetadataReferenceSerializerTest {


    private static ObjectMapper getMapper() {
        ObjectMapper mapper = new ObjectMapper();
        mapper.setPropertyNamingStrategy(PropertyNamingStrategies.UPPER_CAMEL_CASE);
        mapper.configure(JsonGenerator.Feature.IGNORE_UNKNOWN, true);
        mapper.configure(JsonGenerator.Feature.WRITE_BIGDECIMAL_AS_PLAIN, true);
        mapper.configure(JsonParser.Feature.ALLOW_MISSING_VALUES, true);
        mapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
        mapper.configure(MapperFeature.ACCEPT_CASE_INSENSITIVE_PROPERTIES, true);//大小写脱敏 默认为false  需要改为tru
        mapper.configure(SerializationFeature.INDENT_OUTPUT, true);
        return mapper;
    }

    /**
     * 测试反序列化null场景
     */
    @Test
    void serializeNull() {
        Assertions.assertDoesNotThrow(() -> {
            getMapper().writeValueAsString(null);
        });

        try {
            Assertions.assertEquals("null", getMapper().writeValueAsString(null));
        } catch (JsonProcessingException e) {
            throw new RuntimeException(e);
        }
    }

    /**
     * 测试序列化json结构内容是否与实体类属性一致
     */
    @Test
    void serialize() {
        MetadataReference metadataReference = new MetadataReference();
        metadataReference.setMetadata(new MetadataHeader());
        metadataReference.setReferenceDetail(new ArrayList<>());
        MetadataHeader dependentMetadata = new MetadataHeader();
        dependentMetadata.setId(UUID.randomUUID().toString());
        dependentMetadata.setCertId(UUID.randomUUID().toString());
        dependentMetadata.setNameSpace(UUID.randomUUID().toString());
        dependentMetadata.setCode(UUID.randomUUID().toString());
        dependentMetadata.setName(UUID.randomUUID().toString());
        dependentMetadata.setType(UUID.randomUUID().toString());
        dependentMetadata.setBizobjectID(UUID.randomUUID().toString());
        metadataReference.setDependentMetadata(dependentMetadata);


        Assertions.assertDoesNotThrow(() -> {
            String string = getMapper().writeValueAsString(metadataReference);
            JsonNode jsonNode = getMapper().readTree(string);

            Assertions.assertNotNull(jsonNode);
            Assertions.assertTrue(jsonNode.isObject());
            Assertions.assertEquals(1, jsonNode.size());
            Assertions.assertNotNull(jsonNode.get("DependentMetadata"));
            jsonNode = jsonNode.get("DependentMetadata");

            Assertions.assertTrue(jsonNode.isObject());
            Assertions.assertEquals(dependentMetadata.getId(), jsonNode.get("ID").asText());
            Assertions.assertEquals(dependentMetadata.getCertId(), jsonNode.get("CertId").asText());
            Assertions.assertEquals(dependentMetadata.getNameSpace(), jsonNode.get("NameSpace").asText());
            Assertions.assertEquals(dependentMetadata.getCode(), jsonNode.get("Code").asText());
            Assertions.assertEquals(dependentMetadata.getName(), jsonNode.get("Name").asText());
            Assertions.assertEquals(dependentMetadata.getType(), jsonNode.get("Type").asText());
            Assertions.assertEquals(dependentMetadata.getBizobjectID(), jsonNode.get("BizobjectID").asText());
        });
    }
}