/*
 * Copyright (c) 2020 - present, Inspur Genersoft Co., Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */

package com.inspur.edp.lcm.metadata.api.entity;

import lombok.Data;

/**
 元数据明细依赖

 */
@Data
public class MetadataReferenceDetail
{
    /**
     字段
     <see cref="string"/>

     */
    private String elementID;

    /**
     被依赖的字段
     <see cref="string"/>

     */
    private String dependentElementID;

    /**
     * clone
     * */
    public final Object clone() {
        MetadataReferenceDetail metadataReferenceDetail = new MetadataReferenceDetail();
        metadataReferenceDetail.setElementID(this.getElementID());
        metadataReferenceDetail.setDependentElementID(this.getDependentElementID());

        return metadataReferenceDetail;
    }
}