/*
 * Copyright (c) 2020 - present, Inspur Genersoft Co., Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */

package com.inspur.edp.lcm.metadata.api.entity.uri;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;
import org.springframework.util.StringUtils;

@Data
public class MetadataURI extends AbstractURI {
    @JsonProperty("ID")
    private String id;
    private String code;
    private String type;
    private String nameSpace;

    public MetadataURI() {
    }

    public MetadataURI(String id) {
        this.id = id;
    }

    public MetadataURI(String id, String code, String type, String nameSpace) {
        this.id = id;
        this.code = code;
        this.type = type;
        this.nameSpace = nameSpace;
    }

    @Override
    public String getURIDesc() {
        return StringUtils.isEmpty(getCode()) ? getId() : (getCode() + "," + getType() + "," + getNameSpace());
    }
}
