/*
 * Copyright (c) 2020 - present, Inspur Genersoft Co., Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */

package com.inspur.edp.lcm.metadata.api.entity;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.inspur.edp.lcm.metadata.api.entity.uri.MetadataURI;
import lombok.Data;
import org.springframework.util.CollectionUtils;
import org.springframework.util.StringUtils;

import java.util.Map;

/**
 * 元数据的头节点，描述元数据的基本信息
 */
@Data
public class MetadataHeader extends MetadataURI {
    // 证书标识，可以是证书编号
    private String certId;

    // 元数据名称
    private String name;

    // 元数据文件名
    private String fileName;

    // 业务对象ID
    private String bizobjectID;

    // 语言
    private String language;

    // 是否为翻译元数据
    @JsonProperty("IsTranslating")
    private boolean isTranslating;

    // 此元数据是否允许扩展
    private boolean extendable;

    private Map<String, String> nameLanguage;

    public MetadataHeader() {
    }

    public MetadataHeader(String id, String certId, String nameSpace, String code, String name, String fileName, String type, String bizobjectID, String language, boolean isTranslating, boolean extendable) {
        setId(id);
        this.certId = certId;
        setNameSpace(nameSpace);
        setCode(code);
        this.name = name;
        this.fileName = fileName;
        setType(type);
        this.bizobjectID = bizobjectID;
        this.language = language;
        this.isTranslating = isTranslating;
        this.extendable = extendable;
    }

    public MetadataHeader(String id, String nameSpace, String code, String name, String type, String bizobjectID) {
        setId(id);
        setNameSpace(nameSpace);
        setCode(code);
        this.name = name;
        setType(type);
        this.bizobjectID = bizobjectID;
    }

    @Override
    public final Object clone() {
        MetadataHeader header = new MetadataHeader();
        header.setId(this.getId());
        header.setBizobjectID(this.getBizobjectID());
        header.setType(this.getType());
        header.setCode(this.getCode());
        header.setName(this.getName());
        header.setNameSpace(this.getNameSpace());
        header.setFileName(this.getFileName());
        header.setLanguage(this.getLanguage());
        header.setTranslating(this.isTranslating());
        header.setExtendable(this.isExtendable());
        header.setCertId(this.getCertId());
        header.setNameLanguage(this.getNameLanguage());
        return header;
    }
    public void setI18nName(String language){
        if(StringUtils.hasText(language) && !CollectionUtils.isEmpty(nameLanguage) && StringUtils.hasText(nameLanguage.get(language))){
            this.setName(nameLanguage.get(language));
        }
    }
}