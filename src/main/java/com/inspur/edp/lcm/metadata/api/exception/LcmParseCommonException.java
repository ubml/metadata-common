/*
 * Copyright (c) 2020 - present, Inspur Genersoft Co., Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.inspur.edp.lcm.metadata.api.exception;

import io.iec.edp.caf.commons.exception.CAFRuntimeException;
import io.iec.edp.caf.commons.exception.ExceptionLevel;

/**
 * desc
 *
 * @author liangff
 * @since
 */
public class LcmParseCommonException extends CAFRuntimeException {
    private static final String SU = "pfcommon";
    private static final String RESOURCE_FILE = "metadata_common_exception.properties";

    public LcmParseCommonException(CommonErrorCodes errCodes, String... messageParams) {
        super(SU, RESOURCE_FILE, errCodes.name(), messageParams, null, ExceptionLevel.Error, true);
    }

    public LcmParseCommonException(Throwable innerException, CommonErrorCodes errCodes, String... messageParams) {
        super(SU, RESOURCE_FILE, errCodes.name(), messageParams, innerException, ExceptionLevel.Error, true);
    }
}
