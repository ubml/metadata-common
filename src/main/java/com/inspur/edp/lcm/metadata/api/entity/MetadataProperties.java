/*
 * Copyright (c) 2020 - present, Inspur Genersoft Co., Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */

package com.inspur.edp.lcm.metadata.api.entity;

import lombok.Data;

@Data
public class MetadataProperties {
    /**
     * 元数据schema版本
     */
    private String schemaVersion;

    /**
     * 元数据缓存版本，用以标识内存缓存版本（bef使用），暂存lastChangedOn
     */
    private String cacheVersion;
    /**
     * 表单元数据所属框架(vue或者ng)
     */
    private FrameWorkTypeEnum framework;

    public MetadataProperties() {
    }

    public MetadataProperties(String schemaVersion, String cacheVersion) {
        this.schemaVersion = schemaVersion;
        this.cacheVersion = cacheVersion;
    }

    @Override
    public Object clone() {
        MetadataProperties properties=new MetadataProperties();
        properties.setSchemaVersion(this.schemaVersion);
        properties.setCacheVersion(this.cacheVersion);
        properties.setFramework(this.framework);
        return properties;
    }
}
