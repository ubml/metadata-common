/*
 * Copyright (c) 2020 - present, Inspur Genersoft Co., Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */

package com.inspur.edp.lcm.metadata.api.entity;

import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.databind.SerializerProvider;
import com.fasterxml.jackson.databind.ser.std.StdSerializer;
import com.inspur.edp.lcm.metadata.api.exception.CommonErrorCodes;
import com.inspur.edp.lcm.metadata.api.exception.LcmParseCommonException;

import java.io.IOException;
import java.util.Objects;

/**
 * The type MetadataReferenceSerializer
 * 自定义MetadataReference序列化
 * @author: Jack Lee
 */
public class MetadataReferenceSerializer extends StdSerializer<MetadataReference> {

    public MetadataReferenceSerializer() {
        super(MetadataReference.class);
    }

    @Override public void serialize(MetadataReference reference, JsonGenerator generator,
        SerializerProvider provider) throws IOException {
        if (Objects.isNull(reference) == false){
            generator.writeStartObject();
            generator.writeFieldName("DependentMetadata");
            generator.writeStartObject();
            writePropertyAndValue(generator, "ID", reference.getDependentMetadata().getId());
            writePropertyAndValue(generator, "CertId", reference.getDependentMetadata().getCertId());
            writePropertyAndValue(generator, "NameSpace", reference.getDependentMetadata().getNameSpace());
            writePropertyAndValue(generator, "Code", reference.getDependentMetadata().getCode());
            writePropertyAndValue(generator, "Name", reference.getDependentMetadata().getName());
            writePropertyAndValue(generator, "Type", reference.getDependentMetadata().getType());
            writePropertyAndValue(generator, "BizobjectID", reference.getDependentMetadata().getBizobjectID());
            generator.writeEndObject();
            //写List<MetadataReferenceDetail> referenceDetail
            writeMetadataReferenceDetail(generator, reference);
            generator.writeEndObject();
        }

    }

    private void writeMetadataReferenceDetail(JsonGenerator generator, MetadataReference reference){
        if (reference.getReferenceDetail() == null || reference.getReferenceDetail().size() < 1){
            return;
        }
        try{
            generator.writeFieldName("ReferenceDetail");
            generator.writeStartArray();
            for (int  i = 0; i < reference.getReferenceDetail().size(); i++){
                MetadataReferenceDetail referenceDetail = reference.getReferenceDetail().get(i);
                generator.writeStartObject();
                writePropertyAndValue(generator, "ElementID", referenceDetail.getElementID());
                writePropertyAndValue(generator, "DependentElementID", referenceDetail.getDependentElementID());
                generator.writeEndObject();
            }
            generator.writeEndArray();
        }
        catch (IOException ioe){
            throw new LcmParseCommonException(ioe, CommonErrorCodes.ECP_PARSE_COMMON_0001);
        }

    }

    private void writePropertyAndValue(JsonGenerator generator, String propertyName, String propertyValue) throws IOException {
        generator.writeFieldName(propertyName);
        generator.writeString(propertyValue);
    }
}
