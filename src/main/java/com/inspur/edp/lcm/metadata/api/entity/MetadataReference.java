/*
 * Copyright (c) 2020 - present, Inspur Genersoft Co., Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */

package com.inspur.edp.lcm.metadata.api.entity;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import java.util.Objects;
import lombok.Data;

import java.util.ArrayList;
import java.util.List;

/**
 元数据的依赖关系

 */
@JsonSerialize(using = MetadataReferenceSerializer.class)
@Data
public class MetadataReference
{
    /**
     源元数据
     <see cref="MetadataHeader"/>

     */
    @JsonIgnore
    private MetadataHeader metadata;

    /**
     被依赖的元数据
     <see cref="MetadataHeader"/>

     */
    private MetadataHeader dependentMetadata;

    /**
     具体的字段依赖
     <see cref="List{T}"/>
     <see cref="MetadataReferenceDetail"/>

     */
    private List<MetadataReferenceDetail> referenceDetail;

    /**
     * clone
     * */
    public Object clone() {
        MetadataReference metadataReference = new MetadataReference();
        if (Objects.nonNull(this.metadata)){
            metadataReference.setMetadata((MetadataHeader) this.metadata.clone());
        }
        if (Objects.nonNull(this.dependentMetadata)){
            metadataReference.setDependentMetadata((MetadataHeader) this.dependentMetadata.clone());
        }
        metadataReference.setReferenceDetail(new ArrayList<>());
        if(this.referenceDetail == null || this.referenceDetail.size() <= 0)
            return metadataReference;
        else {
            this.referenceDetail.forEach(item -> {
                metadataReference.getReferenceDetail().add((MetadataReferenceDetail) item.clone());
            });

            return metadataReference;
        }
    }
}