# metadata-common

### 介绍
UBML元数据模型公共结构，里面包含元数据结构、基本信息、依赖关系等内容。

### 软件架构
![Architecture](./imgs/gspmetadata.png)

### 特性

* 元数据包含元数据基本信息、元数据依赖关系、元数据内容等。
* 元数据基本信息包含唯一标识、名称、编号、命名空间、类型等基础信息。
* 元数据依赖关系用于直接描述元数据对其他元数据的依赖。
* 元数据结构使用组合关系，组合具体元数据内容。基于组合结构，各类元数据可以灵活定义自身内容。元数据内容接口属于标识接口，具体类型的元数据，可以实现自己的具体结构。

### 使用

各类模型，元数据服务依赖于元数据模型公共结构。

#### Maven dependency

```xml
<properties>
    <metdata.common.version>0.1.0</metdata.common.version>
</properties>
```
```xml
<dependencies>
    <dependency>
        <groupId>com.inspur.edp</groupId>
        <artifactId>metadata-common</artifactId>
        <version>${metdata.common.version}</version>
    </dependency>
</dependencies>
```
### 关于新增元数据
1. 基于元数据框架，自定义新增元数据的content结构：
   实现com.inspur.edp.lcm.metadata.api.entity.GspMetadata类里面的
   com.inspur.edp.lcm.metadata.api.IMetadataContent接口
   
   （1） 添加maven依赖
   ```xml
   <dependency>
        <groupId>com.inspur.edp</groupId>
        <artifactId>metadata-common</artifactId>
        <version>0.1.0</version>
   </dependency>
   ```
   （2）定义元数据的content结构，继承IMetadataContent接口的抽象实现类
   com.inspur.edp.lcm.metadata.api.AbstractMetadataContent，以业务实体GspBusinessEntity为例：
   
   ![Architecture](./imgs/gspbusinessentity.png)
   ![Architecture](./imgs/gspcommonmodel.png)
    
2.实现元数据内容初始化：
实现com.inspur.edp.lcm.metadata.spi.MetadataContentManager#build接口，完成新增元数据content的初始化。

   （1） 添加maven依赖
   ```xml
   <dependency>
        <groupId>com.inspur.edp</groupId>
        <artifactId> metadata-service-dev-spi </artifactId>
        <version>0.1.0</version>
   </dependency>
   ```
   （2） 实现扩展接口，以业务实体为例：

![Architecture](./imgs/build.png)

3.实现新增元数据的序列化器、反序列化器：
实现com.inspur.edp.lcm.metadata.spi.MetadataContentSerializer#Serialize接口，完成新增元数据content的序列化服务。
实现com.inspur.edp.lcm.metadata.spi.MetadataContentSerializer#DeSerialize接口，完成新增元数据content的反序列化服务。

（1）    添加maven依赖
   ```xml
   <dependency>
        <groupId>com.inspur.edp</groupId>
        <artifactId>metadata-common</artifactId>
        <version>0.1.0</version>
   </dependency>
   ```
（2）	实现扩展接口,以业务实体为例：

![Architecture](./imgs/serialize.png)

4.	实现新增元数据的引用关系扩展服务：
      实现com.inspur.edp.lcm.metadata.spi.IMetadataReferenceManager接口，完成新增元数据的引用关系扩展服务。
      
      （1）	添加maven依赖
      ```xml
      <dependency>
            <groupId>com.inspur.edp</groupId>
            <artifactId> metadata-service-dev-spi</artifactId>
            <version>0.1.0</version>
      </dependency>
      ```
      （2）	实现扩展接口,以业务实体为例：

![Architecture](./imgs/reference.png)

5.  初始新增元数据的配置信息：
    
      在server\config\platform\common\lcm_metadataextend.json配置文件中, 需要实现两个配置：
    
    （1）MetadataConfiguration配置节，Common为基本信息，Serializer与TransferSerializer为实现的两个序列化配置对应的jar包及实现类，Manager为实现的初始化实体对应jar包及实现类。
    
![Architecture](./imgs/config1.png)
    （2）MetadataType配置节，需要配置元数据的编码、名称、后缀以及分组等信息。

![Architecture](./imgs/config2.png)

### 参与贡献
参与贡献，请参照[如何贡献](https://gitee.com/ubml/community/blob/master/zh-cn/CONTRIBUTING.md)获取帮助。

### License
[Apache License 2.0](https://gitee.com/ubml/metadata-common/blob/master/LICENSE)