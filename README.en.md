# metadata-common

### Introduction
UBML metadata model common structure, including metadata structure, basic information, dependency relationships, and more.
### Software Architecture
![Architecture](./imgs/gspmetadata.png)

### Features

* Metadata includes basic information, dependency relationships, content, and more.
* Basic information of metadata includes unique identifier, name, number, namespace, type, and other basic information.
* Dependency relationships describe the direct dependencies of metadata on other metadata.
* Metadata structure uses composition to combine specific metadata content. Based on the composition structure, various types of metadata can flexibly define their own content. The metadata content interface is an identification interface, and specific types of metadata can implement their own specific structures.

### Usage

Various models and metadata services depend on the metadata model common structure.

#### Maven dependency

```xml
<properties>
    <metdata.common.version>0.1.0</metdata.common.version>
</properties>
```
```xml
<dependencies>
    <dependency>
        <groupId>com.inspur.edp</groupId>
        <artifactId>metadata-common</artifactId>
        <version>${metdata.common.version}</version>
    </dependency>
</dependencies>
```
### 关于新增元数据
1. Customizing Metadata Content Structure:
   Extend the com.inspur.edp.lcm.metadata.api.entity.GspMetadata class and implement the com.inspur.edp.lcm.metadata.api.IMetadataContent interface to define the structure of your custom metadata content.
   
   (1) Maven Dependency
   ```xml
   <dependency>
        <groupId>com.inspur.edp</groupId>
        <artifactId>metadata-common</artifactId>
        <version>0.1.0</version>
   </dependency>
   ```
   (2) Define the content structure of metadata, inherit the abstract implementation class com.inspur.edp.lcm.metadata.api.AbstractMetadataContent of the IMetadataContent interface, and take the business entity GspBusinessEntity as an example：
   
   ![Architecture](./imgs/gspbusinessentity.png)
   ![Architecture](./imgs/gspcommonmodel.png)

2.Initializing Metadata Content：
Implement the com.inspur.edp.lcm.metadata.spi.MetadataContentManager#build method to initialize the content of new metadata entries.

(1) Maven Dependency
   ```xml
   <dependency>
        <groupId>com.inspur.edp</groupId>
        <artifactId> metadata-service-dev-spi </artifactId>
        <version>0.1.0</version>
   </dependency>
   ```
(2) An illustration demonstrates this step with a focus on business entities:

![Architecture](./imgs/build.png)

3.Implementing Serialization and Deserialization Services：
For serialization, implement com.inspur.edp.lcm.metadata.spi.MetadataContentSerializer#Serialize.
For deserialization, implement com.inspur.edp.lcm.metadata.spi.MetadataContentSerializer#Deserialize.

(1)    Maven Dependency
   ```xml
   <dependency>
        <groupId>com.inspur.edp</groupId>
        <artifactId>metadata-common</artifactId>
        <version>0.1.0</version>
   </dependency>
   ```
(2)	An example diagram showcases the implementation for a business entity's serialize:

![Architecture](./imgs/serialize.png)

4.	Extending Reference Relationship Services for Metadata:
      Implement the com.inspur.edp.lcm.metadata.spi.IMetadataReferenceManager interface to provide extended services for managing reference relationships in new metadata.

      (1)	Maven Dependency
      ```xml
      <dependency>
            <groupId>com.inspur.edp</groupId>
            <artifactId> metadata-service-dev-spi</artifactId>
            <version>0.1.0</version>
      </dependency>
      ```
      (2)	An illustration exemplifies this step in the context of business entities:
      
      ![Architecture](./imgs/reference.png)
5.  Initial configuration information for adding metadata:

    In the server\config\platform\common\lcm_metadataextend.json configuration file, two configurations need to be implemented:
    
（1）The MetadataConfiguration section includes: Common for basic information, Serializer and TransferSerializer for the JAR packages and implementation classes of the two serialization configurations, and Manager for the JAR package and implementation class of the initialized entity.


![Architecture](./imgs/config1.png) 
    （2）The MetadataType section requires configuring the code, name, suffix, and grouping information for the metadata.

![Architecture](./imgs/config2.png)
### Contributing
To contribute, please refer to [How to Contribute](https://gitee.com/ubml/community/blob/master/CONTRIBUTING.md) for assistance.

### License
[Apache License 2.0](https://gitee.com/ubml/metadata-common/blob/master/LICENSE)